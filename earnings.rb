#!/usr/bin/env ruby
# frozen_string_literal: true

require 'json'
require 'csv'

START_OF_YEAR = '2021-01-01'
API_CREDS = "#{ENV['H1_API_USERNAME']}:#{ENV['H1_API_TOKEN']}"

url = 'https://api.hackerone.com/v1/hackers/payments/payouts'

data = []
loop do
  response = JSON.parse(`curl -s #{url} -u #{API_CREDS}`, symbolize_names: true)
  new_data = response[:data].select { |x| x[:paid_out_at] >= START_OF_YEAR }
  break if new_data.empty? # Assumes the API returns data in reverse chronological order

  data.push(*new_data)
  url = response[:links][:next]
  break unless url
end

year_to_date = data.sum { |x| x[:amount] }
puts format('Year to date: %.2f USD', year_to_date)

return unless ARGV[0] == '--csv'

# Dump to CSV for more number crunching!
CSV.open('earnings.csv', 'w') do |csv|
  data.each do |hash|
    csv << hash.values
  end
end
