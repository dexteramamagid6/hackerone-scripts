#!/usr/bin/env ruby
# frozen_string_literal: true

require 'httparty'
require 'json'
require 'base64'

API_URL = 'https://hackerone.com/graphql'
HEADERS = {
  "content-type": 'application/json',
  "X-Auth-Token": ARGV[0]
}.freeze

def query(body)
  response = HTTParty.post(API_URL, body: JSON.generate(body), headers: HEADERS)

  abort(response['errors'].join) unless response['errors'].nil?

  response
end

cursor = ''
loop do
  response = query(
    {
      operationName: 'GetTeams',
      variables: '',
      query: "
        query GetTeams {
          teams(
            after: \"#{cursor}\"
            first: 50
            where: {
              _and: [
                { state: { _eq: soft_launched } }
                {
                  _or: [
                    { submission_state: { _eq: paused } }
                    { submission_state: { _eq: disabled } }
                  ]
                }
              ]
            }
          ) {
            pageInfo {
              hasNextPage
            }
            edges {
              cursor
              node {
                handle
                url
                submission_state
                reports_received_last_90_days
              }
            }
          }
        }"
    }
  )

  response['data']['teams']['edges'].each do |edge|
    cursor = edge['cursor']
    node = edge['node']

    puts "#{node['url']} (#{node['submission_state']}, reports received in the last 90 days: #{node['reports_received_last_90_days']})"
  end

  break unless response['data']['teams']['pageInfo']['hasNextPage']
end
