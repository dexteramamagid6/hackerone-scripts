#!/usr/bin/env ruby
# frozen_string_literal: true

require 'httparty'
require 'json'
require 'base64'

abort("Call with X-Auth-Token as first argument") unless ARGV[0]

API_URL = 'https://hackerone.com/graphql'
HEADERS = {
  "content-type": 'application/json',
  "X-Auth-Token": ARGV[0]
}.freeze

def query(body)
  response = HTTParty.post(API_URL, body: JSON.generate(body), headers: HEADERS)

  errors = response['errors'] || []
  # Some programs return Null and that's invalid according to the schema, we can ignore those errors
  errors = errors.select { |e| e['message'] != 'Cannot return null for non-nullable field PolicySetting.subscribed' }
  abort(errors.join) unless errors.empty?

  response
end

def subscribe(team_handle)
  query(
    {
      operationName: 'UpdateSubscription',
      variables: { handle: team_handle },
      query: "
        mutation UpdateSubscription($handle: String!) {
          toggleTeamUpdatesSubscription(input: {handle: $handle}) {
            was_successful
          }
        }"
    }
  )

  puts "[+] Subscribed to #{team_handle}"
end

cursor = ''
loop do
  puts "[*] Querying for 50 programs starting at #{cursor.empty? ? '0' : Base64.decode64(cursor)}"
  pp cursor
  response = query(
    {
      operationName: 'GetTeams',
      variables: '',
      query: "
        query GetTeams {
          teams(after: \"#{cursor}\", first: 50) {
            pageInfo {
              hasNextPage
            }
            edges {
              cursor
              node {
                handle
                state
                offers_bounties
                policy_setting {
                  subscribed
                }
              }
            }
          }
        }"
    }
  )

  response['data']['teams']['edges'].each do |edge|
    cursor = edge['cursor']
    node = edge['node']
    policy_setting = node['policy_setting']
    next if policy_setting.nil? || policy_setting['subscribed']
    # Comment out the next line if you want to also include public programs
    next unless node['state'] == 'soft_launched'
    # Comment out the next line if you want to include VDPs
    next unless node['offers_bounties']

    subscribe(node['handle'])
  end

  break unless response['data']['teams']['pageInfo']['hasNextPage']
end

puts '[+] Done!'
